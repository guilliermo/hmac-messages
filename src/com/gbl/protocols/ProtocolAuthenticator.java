package com.gbl.protocols;

import java.io.IOException;

import com.gbl.client.Client;
import com.gbl.protocols.signals.LeaveProtocolSignal;
import com.gbl.protocols.signals.ProtocolException;
import com.gbl.sessions.Session;
import com.gbl.utils.Message;
import com.gbl.utils.Show;

public class ProtocolAuthenticator extends Protocols
{
	public ProtocolAuthenticator(Session _session)
	{
		super(_session);
	}
	
	protected void doAuthenticationProtocol(String str)
	{
		Message response;
		
		try
		{
			Message.sendStream(Message.createServerMessage(Protocols.StatusResponse.READY.toString()), out);
			String nonce = Message.getNONCE(in,session);
			Message endresponse;
			
			while(true)
			{
				
				Show.trace("ProtocolAuthenticator.doAuthenticationProtocol","LOGIN\n  Enter your login:");
				String login = sc.nextLine();
				Show.test(login,3);
				Show.trace("ProtocolAuthenticator.doAuthenticationProtocol","PASSWORD\n  Enter your password:");
				String password = sc.nextLine();
				Show.test(password+"\n",3);
				
				Show.test("ProtocolAuthenticator","login :"+login+", password: "+password+", nonce: "+nonce,2);
				String authen = Message.sendAUTH(out, login, password, nonce);
				
				response = Message.receiveStream(in,session);
				if(!response.isServerMessage())
				{
					//Show.error("ProtocolAuthenticator.doAuthenticationProtocol","Not a Server Message");
					throw new ProtocolException("- Not a Server Message");
				}
				//response = Message.decodeServeurMessage(response);
				
				if(Protocols.isProtocolStatus(response.getText(), Protocols.StatusResponse.RETRY))
					continue;
				
				else if(Protocols.isProtocolStatus(response.getText(), Protocols.StatusResponse.VALIDATED))
				{
					session.setKeys(authen,nonce);
					session.setLogin(login);
					Client.updateSessionInstance(session);
					break;
				}
				
				else if(Protocols.isProtocolStatus(response.getText(), Protocols.StatusResponse.FAILED))
					throw new LeaveProtocolSignal("***  Authentication failed !");
				
				else
					Show.error("ProtocolAuthenticator.doAuthenticationProtocol","Not a status reponse");
			}
			
			//r�cup�re le READY pour continuer
			endresponse = Message.receiveStream(in,session);
			if(!endresponse.isServerMessage())
			{
				//Show.error("ProtocolAuthenticator.doAuthenticationProtocol","Not a Server Message");
				throw new ProtocolException("- Not a Server Message");
			}
			//endresponse = Message.decodeServeurMessage(endresponse);
			if(!Protocols.isProtocolStatus(endresponse.getText(), Protocols.StatusResponse.READY))
				throw new ProtocolException("- Not a 'READY'status Message");
			else
			{
				Show.test("ProtocolAuthenticator","normally ending protocol",1);
				Show.trace("*** ","Server is ready  ***");
			}
		} catch (IOException e)
		{
			Show.error("ProtocolAuthenticator.doAuthenticationProtocol",e.getLocalizedMessage());
		}
		finally 
		{ 
			//while(sc.hasNextLine())
			//try{
			//	Show.test("ProtocolAuthenticator","scanner: "+sc.nextLine(),3);
			//}catch (Exception e){Show.error("ProtocolAuthenticator",e.getMessage());}
			Show.test("ProtocolAuthenticator","Exiting protocol",3);
		}
	}
	
}
