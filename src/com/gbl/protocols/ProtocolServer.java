package com.gbl.protocols;

import java.io.IOException;

import com.gbl.protocols.signals.BreakProtocolSignal;
import com.gbl.sessions.Session;
import com.gbl.utils.Message;
import com.gbl.utils.Show;

public class ProtocolServer extends Protocols
{
	
	public ProtocolServer(Session _session)
	{
		super(_session);
	}
	
	protected void doMirrorServerProtocol(String str)
	{
		//String prot = "MS";
		try
		{
			Show.test("ProtocolServer.doMirrorServerProtocol","SENDING startresponse",3);	//TEST
			Message.sendStream(Message.createServerMessage(StartResponse.START_MIRROR_SERVER.toString()), out);
//			out.println(StartResponse.START_MIRROR_SERVER.toString());
//			out.flush();
			Show.test("ProtocolServer.doMirrorServerProtocol","after startresponse",3);	//TEST
			
			//Message message;
			while(/*message != null*/ true)
			{
				Message message = Message.receiveStream(in,session);
				Show.test("ProtocolServer.doMirrorServerProtocol",message.getText(),1);
				if(!message.getText().equals(StatusResponse.BREAK.toString()))
				{
					Show.test("ProtocolServer.doMirrorServerProtocol","before sending message= "+message.getText(),2);	//TEST
					//Show.test("ProtocolServer.doMirrorServerProtocol","MAC: "+Security.generateMAC(message),1);
					Message.sendStream(
							Message.createClientMessage(
									message.getText(), "Server", "unknwon", Order.MIRROR_SERVER.toString(),session),
							out);
//					out.println(prot+"|Serveur:: "+message);
//					out.flush();
				}
				else
				{
					Show.test("Protocolserver.doMirrorServerProtocol","statusresponse=BREAK",1);	//TEST
					throw new BreakProtocolSignal();
				}
			}
		}catch (IOException e)
		{ 
			Show.error("Protocolserver.doMirrorServerProtocol",e.getMessage());
			Protocols.sendFail(out);
		}
	}

}