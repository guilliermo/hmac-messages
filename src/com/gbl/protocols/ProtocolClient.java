package com.gbl.protocols;

import java.io.IOException;

import com.gbl.commands.CommandProtocolClient;
import com.gbl.commands.signals.StopCommandSignal;
import com.gbl.sessions.Session;
import com.gbl.utils.Message;
import com.gbl.utils.Show;

public class ProtocolClient extends Protocols
{
	public ProtocolClient(Session _session)
	{
		super(_session);
	}
	
	protected void doMirrorServerProtocol(String str)
	{	
		CommandProtocolClient cpc = new CommandProtocolClient(session);
		
		//try
		//{
			Show.test("ProtocolClient.doMirrorServerProtocol","before order",3);	//TEST
			//initialize a protocol connexion
			Message.sendStream(Message.createServerMessage(Order.MIRROR_SERVER.toString()), out);
//			out.println(Order.MIRROR_SERVER.toString());
//			out.flush();
			
			//if server is ready to continue
			Message response = new Message("");
			try
			{
				response = Message.receiveStream(in,session);
			} catch (IOException e1)	{	Show.error("ProtocolClient.doMirrorServerProtocol",e1.getMessage());	}
			
			Show.test("ProtocolClient.doMirrorServerProtocol","server is ready, response: "+response.getOriginalStringMessage(),1);	//TEST
			if(response.getText().equals(StartResponse.START_MIRROR_SERVER.toString()))
			{
				String messagetosend;
				Message messagereceived;
				while(true)
				{
					try
					{
						
						messagetosend = sc.nextLine();
						Show.test("ProtocolClient.doMirrorServerProtocol","after sc.readline, message: "+messagetosend,3);	//TEST
						
						if(!cpc.executeCmd(messagetosend))
						{
							Message.sendStream(
									Message.createClientMessage(
											messagetosend, "unknwon" /*session.getLogin()*/, "Serveur", Order.MIRROR_CLIENT.toString(),session),
							out);
//							out.println(messagetosend);
//							out.flush();
							Show.test("ProtocolClient.doMirrorServerProtocol","after out.println",3);	//TEST
							
							messagereceived = Message.receiveStream(in,session);
//							if(!Message.analyse(messagereceived))
//								throw new ProtocolException("the analysis of a message detected wrong MAC");
							Show.test("ProtocolClient.doMirrorServerProtocol","messagereceived: "+messagereceived,1);	//TEST
							//if(!messagereceived.equals(StatusResponse.FAILED.toString()))
							if(!Protocols.isProtocolStatus(messagereceived.getText(),StatusResponse.FAILED))
							{
								Show.trace(">> Server:: ",messagereceived.getText()+"	MAC: "+messagereceived.getMac());
							}
							else
							{
								Show.error("ProtocolClient.doMirrorServerProtocol","'FAILED' signal received, Protocol canceled");
								break;
							}
						}
					} catch (IOException e) {
						Show.error("ProtocolClient.doMirrorServerProtocol",e.getMessage());
					//} catch (ExitCommandSignal e) {
					//	Show.trace("ProtocolClient.doMirrorServerProtocol",e.getMessage());
					} catch (StopCommandSignal e) {
						Show.trace("ProtocolClient.doMirrorServerProtocol",e.getMessage());
						break;
//					} catch (Exception e) {
//						Show.error("ProtocolClient.doMirrorServerProtocol",e.getMessage()+e.getCause());
//						//e.printStackTrace();
					}
				}
			}
		/*} catch (IOException e)
		{
			Show.error("ProtocolClient.doMirrorServerProtocol",e.getMessage());
			Protocols.interruptProtocol(out);
		} catch (ExitCommandSignal e) {		Show.trace("protcli.: "+e.getMessage());
		} catch (StopCommandSignal e) {		Show.trace("protcli.: "+e.getMessage());
		}
		*/
	}
	

}
