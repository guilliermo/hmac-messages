package com.gbl.protocols.signals;

@SuppressWarnings("serial")
public class ProtocolException extends RuntimeException
{
	protected static final String message = "Protocol exception";
	
	public ProtocolException(String _message)
	{
		super(_message);
	}
	
	public ProtocolException()
	{
		super(message);
	}
}
