package com.gbl.protocols.signals;

@SuppressWarnings("serial")
public class ProtocolSignal extends RuntimeException
{
	protected static final String message = "Protocol signal invoked";
	
	public ProtocolSignal(String _message)
	{
		super(_message);
	}
	
	public ProtocolSignal()
	{
		super(message);
	}
}
