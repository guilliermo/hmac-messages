package com.gbl.protocols.signals;

@SuppressWarnings("serial")
public class BreakProtocolSignal extends ProtocolSignal
{
	protected static final String message = "'Break protocol' signal invoked";
	
	public BreakProtocolSignal(String _message)
	{
		super(_message);
	}
	
	public BreakProtocolSignal()
	{
		super(message);
	}
}
