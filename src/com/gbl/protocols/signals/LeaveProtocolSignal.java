package com.gbl.protocols.signals;

@SuppressWarnings("serial")
public class LeaveProtocolSignal extends ProtocolSignal
{
	protected static final String message = "'Leave protocol' signal raised";
	
	public LeaveProtocolSignal(String _message)
	{
		super(_message);
	}
	
	public LeaveProtocolSignal()
	{
		super(message);
	}
}
