package com.gbl.sessions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Session
{
	protected PrintWriter out;
	protected BufferedReader in;
	protected Scanner sc;
	protected Socket socket;
	protected String login;
	protected SessionKeys sessionK;
	
	public Session(PrintWriter _out, BufferedReader _in, Scanner _sc, Socket _socket)
	{
		this.out = _out;
		this.in = _in;
		this.sc = _sc;
		this.socket = _socket;
		 
		 this.login = "default";
	}

	public void close() throws IOException
	{
		this.socket.close();
		this.sc.close();
		this.in.close();
		this.out.close();
	}

	/**
	 * @return the out
	 */
	public PrintWriter getOut()
	{
		return out;
	}

	/**
	 * @return the in
	 */
	public BufferedReader getIn()
	{
		return in;
	}

	/**
	 * @return the sc
	 */
	public Scanner getSc()
	{
		return sc;
	}

	/**
	 * @return the socket
	 */
	public Socket getSocket()
	{
		return socket;
	}
	
	/**
	 * @return the login
	 */
	public String getLogin()
	{
		return login;
	}

	/**
	 * @param login the login to set
	 */
	public void setLogin(String login)
	{
		this.login = login;
	}
	
	public void setKeys(String authenStr, String nonce)
	{
		this.sessionK = new SessionKeys(authenStr,Integer.parseInt(nonce));
	}
	
	public String[] getKeys()
	{
		String keys[] = new String[2];
		keys[0] = this.sessionK.getKeyOne();
		keys[1] = this.sessionK.getKeyTwo();
		return keys;
	}
	
}
