package com.gbl.sessions;

import com.gbl.utils.Security;
import com.gbl.utils.Show;

public final class SessionKeys
{
	private String authentication;
	private String keyOne;
	private String keyTwo;
	
	
	protected SessionKeys(String authenticationStr, int nonce)
	{
		this.authentication = Security.hashString(authenticationStr);
		Show.test("SessionKeys","authentication: "+authentication+" | length: "+authentication.length(),7);
		
//		Random rand = new Random();
		int tempint[] = new int[2];
		
//		String nonce1 = Security.hashString(
//							new Timestamp(Calendar.getInstance().getTime().getTime()).toString().trim() );
		
		String nonce1 = "99B9EFFE25F8B57F98AFCE172AB8E7162AB3E79FF710DEEEAD6E6DD714DA1032F7A7A65415F91CD32AB5334345FFB0FDAA8F3C77F645264CFA555E3ABBE10F8B";
		
		Show.test("SessionKeys","nonce1: "+nonce1,6);
		tempint[0] = 8+Integer.parseInt(""+String.valueOf(nonce).charAt(0));	//rand.nextInt(25)+10;
		tempint[1] = 24+Integer.parseInt(""+String.valueOf(nonce).charAt(2));	//rand.nextInt(27)+36;
		
		nonce1 = nonce1.substring(tempint[0], tempint[1]);
		Show.test("SessionKeys","nonce1: "+nonce1+" | 0: "+tempint[0]+", 1: "+tempint[1],6);
		
		tempint[0] = 48+Integer.parseInt(""+String.valueOf(nonce).charAt(1));	//rand.nextInt(23)+64;
		tempint[1] = 73+Integer.parseInt(""+String.valueOf(nonce).charAt(3));	//rand.nextInt(29)+88;
		
		this.keyOne = this.authentication.replaceFirst(this.authentication.substring(tempint[0], tempint[1]), nonce1);
		
		this.keyTwo = ""+((nonce*8369)%9001+1000)+this.authentication.substring(6, this.authentication.length());
		this.keyTwo = this.keyTwo.replace("a", "");
		this.keyTwo = this.keyTwo.replace("e", "a");
		
		this.keyOne = Security.hashString(this.keyOne);
		this.keyTwo = Security.hashString(this.keyOne);
		Show.test("SessionKeys","FINAL:: authen: "+authentication,6);
		Show.test("SessionKeys","FINAL:: keyOne: "+keyOne,5);
		Show.test("SessionKeys","FINAL:: keyTwo: "+keyTwo,5); 
		
	}
	
	protected String getKeyOne()
	{
		return keyOne;
	}
	
	protected String getKeyTwo()
	{
		return keyTwo;
	}

}
