package com.gbl.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.LinkedHashMap;

import com.gbl.protocols.Protocols;
import com.gbl.protocols.signals.ProtocolException;
import com.gbl.sessions.Session;

/**
 * 
 * @author Guillaume Barthelemy
 * 
 */
public class Message
{
	private static final String startServerMessage = "<:::|";
	private static final String endServerMessage = "|:::>";
	private static final String metaTag = "<_:::->";
	private static final String metaConcat = ";;";
	private static final String startMac = "<::-::>";
	private static final String nonceConcat = "::";
	private static final String authConcat = "::";
	private static final String metaSep = "::";
	
	private String originalString;
	private LinkedHashMap<String,String> metaDict;
	private String text;
	private String mac;
	
	
	public Message(String str)
	{
		this.originalString = str;
		
		if(isClientMessage(str))
		{
			try
			{
				Show.test("Message.Message","before array - Startmac: "+startMac,5);
				String array[] = str.split(startMac);
				
				/// TEST ///
				for(int i=0; i<array.length;i++)
				{
					Show.test("Message.Message","array["+i+"]: "+array[i],6);
				} /// TEST ///
				this.mac = array[1];
				
				Show.test("Message.Message","before nomac",5);
				String noMac[] = array[0].split(metaTag);
				this.text = noMac[0];
				
				Show.test("Message.Message","before notext",5);
				String noText[] = noMac[1].split(metaConcat);				
				this.metaDict = new LinkedHashMap<String, String>();
				String meta[];
				Show.test("Message.Message","after meta",5);
				for(int i=0;i<noText.length;i++)
				{
					meta = noText[i].split(metaSep);
					metaDict.put(transform(meta[0]), meta[1]);
				}
			} catch (ArrayIndexOutOfBoundsException e)
			{
				Show.error("Message.Message","client message - Index out of bound:"+e.getMessage());
			}
			Show.test("Message.Message","client message",4);
		}
		else if(isServerMessage(str))
		{
			this.text = decodeServeurMessage(str);
			this.mac = "";
			this.metaDict = new LinkedHashMap<String, String>();
			Show.test("Message.Message","server message",4);
		}
		else
		{
			this.text = str;
			this.mac = "";
			this.metaDict = new LinkedHashMap<String, String>();
			Show.test("Message.Message","Not server nor client message",4);
		}
	}
	
	public String getText()
	{
		return this.text;
	}
	
	public String getMac()
	{
		return this.mac;
	}
	
	public LinkedHashMap<String,String> getMetas()
	{
		return this.metaDict;
	}
	
	public String getOriginalStringMessage()
	{
		return this.originalString;
	}
	
	public boolean isServerMessage()
	{
		return Message.isServerMessage(this.originalString);
	}
	
	public boolean isClientMessage()
	{
		return Message.isClientMessage(this.originalString);
	}
	
	private String transform(String title)
	{
		if(title.equalsIgnoreCase("exped"))
			return "sender";
		else if(title.equalsIgnoreCase("dest"))
			return "recipient";
		else if(title.equalsIgnoreCase("prot"))
			return "protocol";
		else if(title.equalsIgnoreCase("ts"))
			return "timestamp";
		else if(title.equalsIgnoreCase("len"))
			return "length";
		else
		{
			Show.error("Message.transform","- title used unknown");
			return null;	
		}
	}
	
	
	/************************************************************************************************/
	/*                                  Static Methods                                              */ 
	/************************************************************************************************/	
	
	public static String createClientMessage(String text, String expediteur, String destinataire, String protocol, Session session)
	{
		if(Protocols.isProtocolOrder(protocol))
		{
			String str = formatClientMessage(text, expediteur, destinataire, protocol);
			String mac = Security.generateMAC(str,session); //g�n�rer le mac
			Show.test("Message.createClientMessage",encodeSignedMessage(str,mac),4);
			return encodeSignedMessage(str,mac);
		}
		Show.test("Message.createClientMessage","Not an Order protocol word",3);
		return null;
	}
	
	public static String createServerMessage(String protocolWord)
	{
		Show.test("Message.createServerMessage",encodeServeurMessage(protocolWord),4);
		return encodeServeurMessage(protocolWord);
	}
	
	public static Message receiveStream(BufferedReader brin, Session session) throws IOException
	{
		String str = brin.readLine();
		Show.test("Message.receiveStream",str,4);
		Message mess = new Message(str);
		if(mess.isClientMessage())
		{
			if(!Message.analyse(mess,session))
				throw new ProtocolException("Wrong MAC detected at message reception");
		}
		return mess;
	}
	
	public static void sendStream(String str, PrintWriter pwout)
	{
		Show.test("Message.sendStream",str,4);
		pwout.println(str);
		pwout.flush();
	}
	
	public static boolean analyse(Message message, Session session)
	{
//		String str = message.getText().trim()+metaTag
//				+"exped"+ metaSep +message.getMetas().get("sender").trim()	+metaConcat
//				+"dest"+ metaSep +message.getMetas().get("recipient").trim()	+metaConcat
//				+"prot"+ metaSep +message.getMetas().get("protocol").trim()	+metaConcat
//				+"ts"+ metaSep +message.getMetas().get("timestamp").trim()	+metaConcat
//				+"len"+ metaSep +message.getText().trim().length();
		
		String str = Message.formatClientMessage(
				message.getText(), message.getMetas().get("sender"), message.getMetas().get("recipient"), 
				message.getMetas().get("protocol"), message.getMetas().get("timestamp")
				);
		Show.test("Message.analyse","\n\tstr verif:   "+str+"\n\toriginalstr: "+message.originalString,5);
		String mac = Security.generateMAC(str,session);
//		str = str.trim()+startMac+mac.trim()
		
		if(message.getMac().equals(mac))
			return true;
		Show.test("Message.analyse","return false",4);
		Show.test("mac verif: "+mac+", message.mac: "+message.getMac(),4);
		return false;
	}
	
	/**
	 * format a string as a protocol message for/from a client without MAC
	 * @param text the string message to format send by the client
	 * @param login the login of the client sending a message
	 * @param clientnumber the number the server gave the client at connexion
	 * @return a formated client message without MAC
	 */
	private static String formatClientMessage(String text, String expediteur, String destinataire, String protocol)
	{	
		return text.trim()+metaTag
				+"exped"+ metaSep +expediteur.trim()	+metaConcat
				+"dest"+ metaSep +destinataire.trim()	+metaConcat
				+"prot"+ metaSep +protocol.toString().trim()	+metaConcat
				+"ts"+ metaSep +new Timestamp(Calendar.getInstance().getTime().getTime()).toString().trim()	+metaConcat
				+"len"+ metaSep +text.trim().length();
	}
	
	/**
	 * format a string as a protocol message for/from a client without MAC
	 * @param text the string message to format send by the client
	 * @param login the login of the client sending a message
	 * @param clientnumber the number the server gave the client at connexion
	 * @return a formated client message without MAC
	 */
	private static String formatClientMessage(String text, String expediteur, String destinataire, String protocol,String timestamp)
	{	
		return text.trim()+metaTag
				+"exped"+ metaSep +expediteur.trim()	+metaConcat
				+"dest"+ metaSep +destinataire.trim()	+metaConcat
				+"prot"+ metaSep +protocol.toString().trim()	+metaConcat
				+"ts"+ metaSep +timestamp.trim()	+metaConcat
				+"len"+ metaSep +text.trim().length();
	}
	
	/**
	 * format a string as a protocol message for/from the server
	 * @param message the message for server
	 * @return a formated serveur message
	 */
	private static String encodeServeurMessage(String message)
	{	
		return startServerMessage+message.trim()+endServerMessage;
	}
	
	/**
	 * give a string message sent by/for the server
	 * @param message the message from server
	 * @return the serveur message
	 */
	public static String decodeServeurMessage(String message)
	{	
		if(isServerMessage(message))
			return message.replace(startServerMessage,"").replace(endServerMessage,"");
		else
			return null;
	}
	
	/**
	 * concat a string for/from a client with MAC
	 * @param message the message from client
	 * @param mac a MAC string to add
	 * @return concated message with MAC
	 */
	private static String encodeSignedMessage(String message,String mac)
	{	
		return message.trim()+startMac+mac.trim();
	}
	
	/**
	 * give a string which is the mac of the client message
	 * @param formated a protocol message from a client
	 * @return a string representing the MAC or null
	 */
	/*public static String getMacFromMessage(String formated)
	{	
		if(formated.contains(startMac))
		{
			if(isServerMessage(formated))
				formated = decodeServeurMessage(formated);
			String array[] = formated.split(startMac);
			return array[1];
		}	
		else
			return null;
	}*/
	
	/**
	 * give a HashMap<String Key, String value> of metadatas from a client message
	 * @param formated a protocol message from a client
	 * @return a string array containing metadatas or null
	 */
	/*public static HashMap<String, String> getMetaFromClientMessage(String formated)
	{	
		if(formated.contains(startMac) && formated.contains(metaTag))
		{
			String array[] = formated.split(startMac);
			String noMac[] = array[0].split(metaTag);
			String noText[] = noMac[1].split(metaConcat);
			HashMap<String, String> dict = new HashMap<String, String>();
			String metas[];
			for(int i=0;i<noText.length;i++)
			{
				metas = noText[i].split(metaSep);
				dict.put(metas[0], metas[1]);
			}
			return dict;
		}
		else
			return null;
	}*/
	
	/**
	 * give a string message sent by a client
	 * @param formated a protocol message from a client
	 * @return the string message sent by a client or null
	 */
	/*public static String getClientMessage(String formated)
	{	
		if(formated.contains(startMac) && formated.contains(metaTag))
		{
			String array[] = formated.split(startMac);
			String messages[] = array[0].split(metaTag);
			return messages[0];
		}
		else
			return formated;
	}*/
	
	public static void sendNONCE(PrintWriter pwout, String string)
	{
//		pwout.println(encodeServeurMessage(
//						Protocols.Order.NONCE.toString()+nonceConcat+string));
//		pwout.flush();
		Message.sendStream(encodeServeurMessage(
								Protocols.Order.NONCE.toString()+nonceConcat+string), 
							pwout);
	}
	
	public static String sendAUTH(PrintWriter pwout, String login, String password, String nonce)
	{
//		pwout.println(encodeServeurMessage(
//						Protocols.Order.AUTHENTICATION.toString()+authConcat+login+authConcat
//																+Security.hashString(concatNONCE(password,nonce))));
//		pwout.flush();
		Message.sendStream(
				encodeServeurMessage(
						Protocols.Order.AUTHENTICATION.toString()+authConcat+login+authConcat
						+Security.hashString(concatNONCE(password,nonce))), 
				pwout);
		Show.test("SendAuth",encodeServeurMessage(Protocols.Order.AUTHENTICATION.toString()+authConcat+login+authConcat
				+Security.hashString(concatNONCE(password,nonce))),3);
		
		return Security.hashString(concatNONCE(password,nonce));
	}
	
	public static String getNONCE(BufferedReader brin, Session session) throws IOException
	{
		Message response = Message.receiveStream(brin,session);
		if(response.isServerMessage())
		{
			//response = decodeServeurMessage(response);
			String[] array = response.getText().split(nonceConcat);
			if(array.length>1 && array[0].equals(Protocols.Order.NONCE.toString()))
				return array[1];	
		}
		return null;
	}
	
	public static String[] getAUTH(BufferedReader brin, Session session) throws IOException
	{
		String[] hello;
		
		Message response = Message.receiveStream(brin,session);
		if(response.isServerMessage())
		{
			//response = decodeServeurMessage(response);
			String[] array = response.getText().split(authConcat);
			if(array.length>2 && array[0].equals(Protocols.Order.AUTHENTICATION.toString()))
			{
				hello = new String[]{array[1], array[2]};
				Show.test("getAuth: ",hello.toString(),3);
				return hello;
			}
		}
		return null;
	}
	
	public static boolean isServerMessage(String str)
	{
		if(str.startsWith(startServerMessage) && str.endsWith(endServerMessage))
			return true;
		else
			return false;
	}
	
	public static boolean isClientMessage(String str)
	{
		if(str.contains(startMac) && str.contains(metaTag))
			return true;
		else
			return false;
	}
	
	public static String concatNONCE(String password, String nonce)
	{
		return nonce+"_"+password+"+"+nonce;
	}
	
}
