package com.gbl.utils;

import java.io.BufferedReader;
import java.io.IOException;

public class Show
{

	private final static boolean isTestOn = false;
	private final static boolean isErrorOn = true;
	private final static int priorityLevel = 10;
	
	private final static String errorStart = "ERROR::";
	private final static String testStart = "TEST::";
	
	/**
	 * print a string in the console
	 * 
	 * @see Show#error(String)
	 * @see Show#test(String)
	 * @see Show#trace(String)
	 * @see Show#log(String, String)
	 * 
	 * @param str
	 */
	public static void log(String str)
	{
		System.out.println(str);
	}
	
	/**
	 * print a concatened string in the console. with string = "'concat' 'str'"
	 * 
	 * @see Show#error(String)
	 * @see Show#test(String)
	 * @see Show#trace(String)
	 * @see Show#log(String)
	 * 
	 * @param concat
	 * @param str
	 */
	public static void log(String concat, String str)
	{
		System.out.println(concat+" "+str);
	}
	
	/**
	 * print a string in the console and to a specific stream (not defined yet) using log(str)
	 * 
	 * @see Show#error(String)
	 * @see Show#test(String)
	 * @see Show#trace(String, String)
	 * @see Show#log(String)
	 * @see Show#log(String, String)
	 * 
	 * @param str
	 */
	public static void trace(String str)
	{
		log(str);
		//affiche dans un conteneur � string du GUI
	}
	
	/**
	 * same as trace(str) using log(concat,str) to concatenate string
	 * 
	 * @see Show#error(String)
	 * @see Show#test(String)
	 * @see Show#trace(String)
	 * @see Show#log(String)
	 * @see Show#log(String, String)
	 * 
	 * @param concat
	 * @param str
	 */
	public static void trace(String concat, String str)
	{
		log(concat,str);
		//affiche dans un conteneur � string du GUI
	}
	
	/**
	 * print a test string if isTestOn true, using log(str)
	 * 
	 * @see Show#error(String)
	 * @see Show#test(String, String)
	 * @see Show#test(String, int)
	 * @see Show#test(String, String, int)
	 * @see Show#trace(String)
	 * @see Show#log(String)
	 * @see Show#log(String, String)
	 * 
	 * @param str
	 */
	public static void test(String str)
	{
		if(isTestOn)
			log(testStart,str);
	}
	
	/**
	 * same as test(str), but only if the priority parameter is lower than the static parameter priorityLevel
	 * 
	 * @see Show#error(String)
	 * @see Show#test(String)
	 * @see Show#test(String, String, int)
	 * @see Show#test(String, String)
	 * @see Show#trace(String)
	 * @see Show#log(String)
	 * @see Show#log(String, String)
	 * 
	 * @param str
	 * @param priority
	 */
	@SuppressWarnings("unused")
	public static void test(String str, int priority)
	{
		if(isTestOn && (priority <= priorityLevel))
			log(testStart,str);
	}
	
	/**
	 * same as test(str), using log(concat,str) to concatenate string
	 * 
	 * @see Show#error(String)
	 * @see Show#test(String)
	 * @see Show#test(String, int)
	 * @see Show#test(String, String, int)
	 * @see Show#trace(String)
	 * @see Show#log(String)
	 * @see Show#log(String, String)
	 * 
	 * @param methodName
	 * @param str
	 */
	public static void test(String methodName, String str)
	{
		if(isTestOn)
			log(testStart,methodName+": "+str);
	}
	
	/**
	 * same as test(str,priority), using log(concat,str) to concatenate string
	 * 
	 * @see Show#error(String)
	 * @see Show#test(String)
	 * @see Show#test(String, int)
	 * @see Show#test(String, String)
	 * @see Show#trace(String)
	 * @see Show#log(String)
	 * @see Show#log(String, String)
	 * 
	 * @param methodName
	 * @param str
	 * @param priority
	 */
	@SuppressWarnings("unused")
	public static void test(String methodName, String str, int priority)
	{
		if(isTestOn && (priority <= priorityLevel))
			log(testStart,methodName+": "+str);
	}
	
	/**
	 * print a test string if isErrorOn true, using log(str)
	 * 
	 * @see Show#error(String, String)
	 * @see Show#test(String)
	 * @see Show#trace(String)
	 * @see Show#log(String)
	 * 
	 * @param str
	 */
	public static void error(String str)
	{
		if(isErrorOn)
			log(errorStart,str);
	}
	
	/**
	 * same as error(str), using log(concat,str) to concatenate string
	 * 
	 * @see Show#error(String)
	 * @see Show#test(String)
	 * @see Show#trace(String)
	 * @see Show#log(String)
	 * @see Show#log(String, String)
	 * 
	 * @param methodName
	 * @param str
	 */
	public static void error(String methodName, String str)
	{
		if(isErrorOn)
			log(errorStart,methodName+": "+str);
	}
	

	public static String readFromStream(BufferedReader _in)
	{	
		char charCur[] = new char[1]; // d�claration d'un tableau de char d'1 �lement, _in.read() y stockera le char lu
		String message = "";
		
		try
		{
			while(_in.read(charCur, 0, 1)!=-1) // attente en boucle des messages provenant du client (bloquant sur _in.read())
			{
				// on regarde si on arrive � la fin d'une chaine ...
				if (charCur[0] != '\u0000' && charCur[0] != '\n' && charCur[0] != '\r')
					message += charCur[0]; // ... si non, on concat�ne le caract�re dans le message
				
				else if(!message.equalsIgnoreCase("")) // juste une v�rification de principe
				{
					if(charCur[0]=='\u0000') // le dernier caract�re �tait '\u0000' (char de terminaison nulle)
						// on envoi le message en disant qu'il faudra concat�ner '\u0000'
						message = message+charCur[0];	
					message = ""; // on vide la chaine de message pour qu'elle soit r�utilis�e
				}
			}
			return message;
		} catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}		
	}
	
	
}
