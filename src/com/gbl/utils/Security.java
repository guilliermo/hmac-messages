package com.gbl.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.gbl.sessions.Session;

public class Security
{
	
	
	public static String generateMAC(String str, Session session)
	{
		String key[] = session.getKeys();
		String ipad = "";
		String opad = "";
		
		try
		{
			
			MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");
			
			if(key[0].length() > 512)
			{
				key[0] = bytesToHex(messageDigest.digest(key[0].getBytes("UTF8")));
				ipad = key[0];
//				opad = key;
			}
			else if(key[0].length() < 512)
			{
				/*
				for(int i=0; i < 512-key.length(); i++)
				{
					key += (char) 0x00;
				}*/
				key[0] = padRight(key[0],512);
				ipad = padRight(key[0],512);
//				opad = padRight(key,512);
			}
			
			if(key[1].length() > 512)
			{
				key[1] = bytesToHex(messageDigest.digest(key[1].getBytes("UTF8")));
//				ipad = key;
				opad = key[1];
			}
			else if(key[1].length() < 512)
			{
				/*
				for(int i=0; i < 512-key.length(); i++)
				{
					key += (char) 0x00;
				}*/
				key[1] = padRight(key[1],512);
//				ipad = padRight(key[0],512);
				opad = padRight(key[1],512);
			}
			
			Show.test("generateMAC","key[0]: "+key[0]+"*, Key[1]"+key[1]+"*",3);
			
			byte[] k_ipad = ipad.getBytes("UTF8");
			byte[] k_opad = opad.getBytes("UTF8");
			for(int i=0; i<512; i++)
			{
				//ipad += (char) 0x36;
				//opad += (char) 0x5c;
				k_ipad[i] ^= 0x36;
				k_opad[i] ^= 0x5c;
			}			
			ipad = bytesToHex(k_ipad); //new String(k_ipad);			
			opad = bytesToHex(k_opad); //new String(k_opad);
			//ipad = key + ipad;
			//opad = key + opad;
			Show.test("generateMAC","ipad: "+ipad+", opad: "+opad,3);
		
			return bytesToHex(
					messageDigest.digest(
							(opad.concat(bytesToHex(
									messageDigest.digest(
											(ipad.concat(str)).getBytes("UTF8"))
							))).getBytes("UTF8")
					));
			
		} catch (UnsupportedEncodingException e)
		{
			Show.error("generateMAC",e.getMessage());
			return null;
		} catch (NoSuchAlgorithmException e)
		{
			Show.error("generateMAC",e.getMessage());
			return null;
		}
		catch (Exception e)
		{
			Show.error("generateMAC",e.getMessage());
			return null;
		}
	}
	
	public static String hashString(String str)
	{
		try
		{
			String key = "key";
			MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");
			messageDigest.update(str.getBytes("UTF8"));
			messageDigest.update(key.getBytes("UTF8"));	
			return bytesToHex(messageDigest.digest());
			
		} catch (UnsupportedEncodingException e)
		{
			Show.error("hashString",e.getMessage());
			return null;
		} catch (NoSuchAlgorithmException e)
		{
			Show.error("hashString",e.getMessage());
			return null;
		}
		catch (Exception e)
		{
			Show.error("hashString",e.getMessage());
			return null;
		}
	}
	
	private static String bytesToHex(byte[] bytes) 
	{
		StringBuffer result = new StringBuffer();
		for (byte byt : bytes)
			result.append(Integer.toString((byt & 0xff) + 0x100, 16).substring(1));
		return result.toString();
	}
	
	private static String padRight(String s, int n)
	{
		return String.format("%1$-" + n + "s", s);
	}
}
