package com.gbl.testsUnits;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import com.gbl.utils.Message;


public class Test_Message
{
	public Message test_message;
	
	public Test_Message()
	{
		String text = "je me prom�ne dans les pr�s avec ma jolie poup�e"+
						"<[:::]>"+
								"exped::login||"+
								"dest::server||"+
								"prot::MIRROR_SERVER||"+
								"ts::29 avril 2013||"+
								"len::12||"+
						"<[:::]>"+
						"<::+::>MAC";
		this.test_message = new Message(text);
	}

	@Test
	public void testGetText()
	{
		assertEquals("je me prom�ne dans les pr�s avec ma jolie poup�e",test_message.getText());
	}

	@Test
	public void testGetMac()
	{
		assertEquals("MAC",test_message.getMac());
	}

	@Test
	public void testGetOriginalStringMessage()
	{
		assertEquals("je me prom�ne dans les pr�s avec ma jolie poup�e"+
				"<[:::]>exped::login||dest::server||prot::MIRROR_SERVER||ts::29 avril 2013||len::12||<[:::]>"+
				"<::+::>MAC",test_message.getOriginalStringMessage());
	}

	@Test
	public void testCreateClientMessage()
	{
		fail("Not yet implemented");
	}

	@Test
	public void testCreateServerMessage()
	{
		fail("Not yet implemented");
	}

}
