package com.gbl.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Scanner;

import com.gbl.commands.CommandClient;
import com.gbl.commands.signals.ExitCommandSignal;
import com.gbl.commands.signals.StopCommandSignal;
import com.gbl.protocols.ProtocolAuthenticator;
import com.gbl.protocols.Protocols;
import com.gbl.protocols.signals.LeaveProtocolSignal;
import com.gbl.protocols.signals.ProtocolException;
import com.gbl.sessions.Session;
import com.gbl.utils.Message;
import com.gbl.utils.Show;

public class Client
{
	public static Socket socket;
	public static BufferedReader in;
	public static PrintWriter out;
	public static Scanner sc = new Scanner(System.in);
	public static Session clientSession;
	private static Object singleton = null;
	
	private Client(int port) throws UnknownHostException, IOException
	{	
		Client.socket = new Socket(InetAddress.getLocalHost(),port);
		Show.trace("*** ","Starting  ***");
		Show.trace("*** ","Asking connexion ...");
		
		Client.in = new BufferedReader (new InputStreamReader (socket.getInputStream()));
		Client.out = new PrintWriter(socket.getOutputStream());
		Client.clientSession = new Session(out, in, sc, socket);
	}
	
	public static void main(String args[])
	{              
//		Socket socket = null;
//		BufferedReader in = null;
//		PrintWriter out = null;
//		Scanner sc = new Scanner(System.in);
//		Session clientSession = null;
//		Client client = null;
		
		try
		{	
			Integer port;
			if(args.length<=0)
				port=new Integer("18000"); // si pas d'argument : port 18000 par d�faut
			else 
				port = new Integer(args[0]); // sinon il s'agit du num�ro de port pass� en argument

			Client.getinstance(port);
//			
//			socket = new Socket(InetAddress.getLocalHost(),port);
//			Show.trace("*** ","Starting  ***");
//			Show.trace("*** ","Asking connexion ...");
//			
//			in = new BufferedReader (new InputStreamReader (socket.getInputStream()));
//			out = new PrintWriter(socket.getOutputStream());
//			clientSession = new Session(out, in, sc, socket);
			
			String message_distant = "";
			String str="";
			CommandClient cc = new CommandClient(Client.clientSession);
			
			//r�cup�re le signal de demande de protocolAuthenticator
			Message message = Message.receiveStream(Client.in,Client.clientSession);
			//if(!Message.isServerMessage(message.getText()))
			if(!message.isServerMessage())
				throw new ProtocolException(message.getText()+" - Not a server message");
			//message = Message.decodeServeurMessage(message);
			
			if(Protocols.isProtocolStartResponse(message.getText(), Protocols.StartResponse.START_AUTHENTICATION))
			{
				ProtocolAuthenticator pa = new ProtocolAuthenticator(Client.clientSession);
				Show.trace("*** ","Authentication ...");
				pa.launchProtocol(Protocols.Order.AUTHENTICATION.toString());
				Show.test("Client: after pa.launch ",3);
				
				//boucle de protocoles
				while(true)
				{
					Show.test("Client: before sc.nextLine ",3);
					str = Client.sc.nextLine();
					Show.test("Client: str=sc.nextLine: ",str,3);
					try
					{
						if(!cc.executeCmd(str))
						{
							Show.test("str= "+str,2);	//TEST
							Client.out.println(str);
							Client.out.flush();
							Show.test("after flush...",3);	//TEST
							message_distant = Client.in.readLine();
							Show.trace(message_distant);
						}
					} catch (StopCommandSignal e)
					{
						Show.trace(e.getMessage());
						Protocols.interruptProtocol(Client.out);
					}
				}
			}
			
		} catch(UnknownHostException e) {	Show.error(e.getMessage());
		} catch(SocketException e) {		Show.error(e.getMessage());
		} catch(IOException e) {    		Show.error(e.getMessage());
		} catch (ProtocolException e) {		Show.trace(e.getMessage());
		} catch (LeaveProtocolSignal e) {	Show.trace(e.getMessage());
		} catch (ExitCommandSignal e) {		Show.trace(e.getMessage());
		//} catch (StopCommandSignal e) {		Show.trace(e.getMessage());
		//} catch (Exception e) { 			Show.error(e.getMessage());
		} finally
		{	
			try
			{
				Protocols.interruptProtocol(Client.out);
				Show.trace("*** ","Closing connexion ...");
				Client.clientSession.close();
				Show.trace("*** ","Closed  ***");
				System.exit(0);
			} catch (Exception e) 
			{	
				Show.error("Client",e.getMessage());	
				System.exit(0);
			}
		}
	}
	
	
	public static void updateSessionInstance(Session _session)
	{
		clientSession = _session;
	}
	
	public static Client getinstance(int port) throws UnknownHostException, IOException
	{
		if(Client.singleton == null)
		{
			return new Client(port);
		}
		return (Client) Client.singleton;
	}

}
