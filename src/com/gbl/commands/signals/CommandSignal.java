package com.gbl.commands.signals;

@SuppressWarnings("serial")
public class CommandSignal extends RuntimeException 
{
	protected static final String message = "Command exception raised";
	
	public CommandSignal(String _message)
	{
		super(_message);
	}
	
	public CommandSignal()
	{
		super(message);
	}
	
}

