package com.gbl.commands.signals;

/**
 * CommandException raised when Exit command is invoked
 * @author Guilliermo
 *
 */
@SuppressWarnings("serial")
public class ExitCommandSignal extends CommandSignal
{
	protected static final String message = "Command 'Exit' invoked";
	
	public ExitCommandSignal(String _message)
	{
		super(_message);
	}
	
	public ExitCommandSignal()
	{
		super(message);
	}
}
