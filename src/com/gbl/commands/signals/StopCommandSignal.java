package com.gbl.commands.signals;

/**
 * CommandException raised when Stop command is invoked
 * @author Guilliermo
 *
 */
@SuppressWarnings("serial")
public class StopCommandSignal extends CommandSignal
{
	protected static final String message = "Command 'Stop' invoked";
	
	public StopCommandSignal(String _message)
	{
		super(_message);
	}
	
	public StopCommandSignal()
	{
		super(message);
	}
}