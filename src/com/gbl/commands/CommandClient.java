package com.gbl.commands;

import com.gbl.protocols.ProtocolClient;
import com.gbl.protocols.Protocols;
import com.gbl.sessions.Session;
import com.gbl.utils.Show;

public class CommandClient extends Commands
{		
	
	public CommandClient(Session _session)
	{
		super(_session);
	}
	
	protected void doCaseMirror(String str)
	{
		Show.test("doCaseMirror: before protocolClient launch - no polymorphic needed",2);
		ProtocolClient pc = new ProtocolClient(session);
		pc.launchProtocol(Protocols.Order.MIRROR_SERVER);
	}
	
	protected void doCaseSend(String str)
	{
		Show.trace(">> Send: protocol not implemented yet.");
	}
	
	protected void doCaseHelp(String str)
	{
		Show.trace("#-----------------------------------------------------------------------#");
		Show.trace("#--               --  CLIENT - COMMAND LIST  --                       --#");
		Show.trace("#-----------------------------------------------------------------------#");
		Show.trace("# /help: the list of commands                                           #");
		Show.trace("# /send: send message to the serveur or another client and get response #");
		Show.trace("# /stop: go back to default exchange message                            #");
		Show.trace("# /mirror: same as '/send' but only get the same message as a response  #");
		Show.trace("# /quit: the list of commands                                           #");
		Show.trace("#-----------------------------------------------------------------------#");
	}
	
	protected void doCaseStop(String str)
	{
		Show.trace(">> STOP: no protocol launched yet.");
	}
	
	protected void doCaseExit(String str)
	{
		
	}
}
