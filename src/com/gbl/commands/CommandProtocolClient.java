package com.gbl.commands;

import com.gbl.protocols.ProtocolClient;
import com.gbl.sessions.Session;
import com.gbl.utils.Show;

public class CommandProtocolClient extends Commands
{
	public CommandProtocolClient(Session _session)
	{
		super(_session);
	}
	
	
	protected void doCaseMirror(String str)
	{
		Show.trace(">> Mirror: a protocol is already launched\n   use command '/stop' and restart a new one.");
	}
	
	protected void doCaseSend(String str)
	{
		Show.trace(">> Send: a protocol is already launched\n   use command '/stop' and restart a new one.");
	}
	
	protected void doCaseStop(String str)
	{
		ProtocolClient.interruptProtocol(out);
	}
	
	protected void doCaseHelp(String str)
	{
		Show.trace("#-----------------------------------------------------------------------#");
		Show.trace("#--            --  CLIENT PROTOCOL - COMMAND LIST  --                 --#");
		Show.trace("#-----------------------------------------------------------------------#");
		Show.trace("# /help: the list of commands                                           #");
		Show.trace("# /send: send message to the serveur or another client and get response #");
		Show.trace("# /stop: go back to default exchange message                            #");
		Show.trace("# /mirror: same as '/send' but only get the same message as a response  #");
		Show.trace("# /quit: the list of commands                                           #");
		Show.trace("#-----------------------------------------------------------------------#");
	}
}
