
package com.gbl.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.Vector;

import com.gbl.sessions.Session;
import com.gbl.utils.Message;
import com.gbl.utils.Security;
import com.gbl.utils.Show;

public class Server
{
	private Vector<PrintWriter> _tabClients = new Vector<PrintWriter>(); 	// contiendra tous les flux de sortie vers les clients
	private int _nbClients=0; 	// nombre total de clients connect�s
	
	
	public static void main(String args[])
	{
		Session serverSession;
		Scanner sc;
		
		Server server = new Server();
		try
		{
			Integer port;
			if(args.length<=0)
				port=new Integer("18000"); // si pas d'argument : port 18000 par d�faut
			else 
				port = new Integer(args[0]); // sinon il s'agit du num�ro de port pass� en argument
					
			@SuppressWarnings("resource")
			ServerSocket ss = new ServerSocket(port.intValue());
			printWelcome(port);
			
			while (true)
			{
				//gestion du client
				// //new HandleClient(ss.accept(),serv); // un client se connecte, un nouveau thread client est lanc�
				Socket socket = ss.accept();
				sc = new Scanner(System.in);
				serverSession = new Session(
						new PrintWriter(socket.getOutputStream()), 
						new BufferedReader(new InputStreamReader(socket.getInputStream())), 
						sc, socket);
				new HandleAuthentication(serverSession,server);
				Show.test("Server Launched a new HandleAuthentication",3);
			}
		}
		catch (Exception e) { }
	}
	
	/**
	 * Print a welcome message in the server console
	 * 
	 * @param port the port in which the server gets the socket request
	 */
	static private void printWelcome(Integer port)
	{
		Show.trace("#-----------------------------------------------------------------------#");
		Show.trace("#--                        --  WELCOME  --                            --#");
		Show.trace("#-----------------------------------------------------------------------#");
		Show.trace("# Server java application by Guillaume Barthelemy                       #");
		Show.trace("# Version: 1.0 - Avril 2013                                             #");
		Show.trace("# Starting on port: "+port.toString()+"		                                #");
		Show.trace("# write '/help' for help on commands                                    #");
		Show.trace("#-----------------------------------------------------------------------#");
	}
	
	/**
	 * Destroy a client in the tabClients Vector synchronizedly
	 * 
	 * @see Server#addClient(PrintWriter)
	 * @see Server#getNbClients()
	 * 
	 * @param i int id of a client to delete
	 */
	synchronized protected void delClient(int i)
	{
		_nbClients--; // un client en moins ! snif
		if (_tabClients.elementAt(i) != null) // l'�l�ment existe ...
		{
			_tabClients.removeElementAt(i); // ... on le supprime
		}
	}

	/**
	 * Add a client in the tabClients Vector synchronizedly
	 * 
	 * @see Server#delClient(int)
	 * @see Server#getNbClients()
	 * 
	 * @param out the OutputStream PrintWriter of a new client
	 * @return the number of clients
	 */
	synchronized protected int addClient(PrintWriter out)
	{
		_nbClients++; // un client en plus ! ouaaaih
		_tabClients.addElement(out); // on ajoute le nouveau flux de sortie au tableau
		return _tabClients.size()-1; // on retourne le num�ro du client ajout� (size-1)
	}
	
	/**
	 * Get the number of clients synchronizedly
	 * 
	 * @see Server#addClient(PrintWriter)
	 * @see Server#delClient(int)
	 * 
	 * @return
	 */
	synchronized protected int getNbClients()
	{
		return _nbClients; // retourne le nombre de clients connect�s
	}
	
	synchronized static protected boolean isValidPassword(String login, String pass, String nonce) 
	{
		boolean connexion = false;
		Scanner sc = null;
		
		Show.test("isValidPassword","hello before try catch",3);
		try 
		{
			sc = new Scanner(new File("ServerFolder/ConfigSession.txt"));
			while(sc.hasNext())
			{
				String[] array = sc.nextLine().split("::");
				Show.test("isValidPassword","login: "+array[0]+"|"+login+", pass: "+Security.hashString(Message.concatNONCE(array[1], nonce))+"|"+pass,2);
				
				if(login.equals(array[0]) && pass.equals(Security.hashString(Message.concatNONCE(array[1], nonce))))
				{
					connexion = true;
					break;
				}
			}  
		} catch (Exception e)
		{
			Show.error("isValidPassword", e.getMessage());
		}
		finally
		{
			while(sc.hasNextLine())
			{
				sc.nextLine();
			}
			sc.close();
		}
		Show.test("isValidPassword",""+connexion,2);
		return connexion; 
	}
	
}
