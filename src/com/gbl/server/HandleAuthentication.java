package com.gbl.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Random;

import com.gbl.protocols.Protocols;
import com.gbl.sessions.Session;
import com.gbl.utils.Message;
import com.gbl.utils.Show;
 
public class HandleAuthentication implements Runnable 
{
 
	private Socket socket;
	private Server server;
	private PrintWriter out = null;
	private BufferedReader in = null;
	//private String login,pass;
	private boolean authentifier = false;
	private Thread thread;
	private final int nbTestLogin = 3;
	private Session serverSession;
     
	public HandleAuthentication(Session _serverSession, Server _server)
    {
		this.server = _server;
		this.serverSession = _serverSession;
		this.out = serverSession.getOut();
		this.in = serverSession.getIn();
		thread = new Thread(this); // instanciation du thread
		thread.start();
    }  

	public void run() 
	{    
		try 
		{   
			/*
				while(!authentifier)
				{
					_out.println("Entrez votre login :");
					_out.flush();
					login = _in.readLine();
					
					_out.println("Entrez votre mot de passe :");
					_out.flush();
					pass = _in.readLine();
					
					if(server.isValidPassword(login, pass))
					{	                 	                
						Show.trace(login,"is identified");
						_out.println("connecte");
						_out.flush();
						
						// un client se connecte, un nouveau thread client est lanc�
						//_t = new HandleClient(ss.accept(),server,login);
						
						authentifier = true;    
					}
					else 
					{
						_out.println(Protocols.StatusResponse.FAILED.toString());
						_out.flush();
					}
				}
			*/
			
			Show.test("Server send Authentication protocol signal",2);
			Message.sendStream(Message.createServerMessage(Protocols.StartResponse.START_AUTHENTICATION.toString()), out);
			
			Message response = Message.receiveStream(in,serverSession);
			//response = Message.decodeServeurMessage(response);
			if(Protocols.isProtocolStatus(response.getText(), Protocols.StatusResponse.READY))
			{
				//envoit d'un NONCE pour communiquer le mot de passe
				String nonce = getNonce();
				Message.sendNONCE(out, nonce);
				Show.test("Nonce sent to client",3);
				
				int cpt = 0;
				while(cpt<nbTestLogin)
				{
					String[] auth = Message.getAUTH(in,serverSession);
					if(auth == null)
						Show.test("auth == null",3);
					Show.test("auth[0]: "+auth[0]+", auth[1]: "+auth[1],3);
					//login=auth[0];
					//pass=auth[1];
					cpt++;
					if(Server.isValidPassword(auth[0],auth[1],nonce))
					{
						Show.test("Session validated",3);
						serverSession.setLogin(auth[0]);
						serverSession.setKeys(auth[1],nonce);
						Message.sendStream(Message.createServerMessage(Protocols.StatusResponse.VALIDATED.toString()), out);
						authentifier = true;
						break;
					}
					else if(cpt<nbTestLogin)
					{
						Message.sendStream(Message.createServerMessage(Protocols.StatusResponse.RETRY.toString()), out);
						Show.trace("	Try authentication... "+cpt);
					}
					else
						break;
				}
				if(authentifier)
				{
					//un nouveau thread client est lanc�
					new HandleClient(serverSession,server);
					Show.test("test ServerSession","login: "+serverSession.getLogin()+", keyOne: "+serverSession.getKeys()[0]+", KeyTwo: "+serverSession.getKeys()[1],4);
					Show.test("Launch HandleClient",2);
				}
				else
				{
					Message.sendStream(Message.createServerMessage(Protocols.StatusResponse.FAILED.toString()), out);
					Show.error("Server could not authenticate the client");
					socket.close();
				}
			}
			else
				Show.error("Not a 'READY' status response");
		} catch (IOException e) 
		{  
			Show.error("Server","had no answer: "+e.getMessage());
			try
			{
				Show.trace("***  Authentication failed, connexion aborted ...");
				socket.close(); // fermeture du socket si il ne l'a pas d�j� �t� (� cause de l'exception lev�e plus haut)
			}
			catch (Exception e2)
			{ 
				Show.error("HandleAuthentication","Can not close ServerSocket\n"+e2.getMessage()); 
			}
		}
	}
	
	private static String getNonce()
	{
		Random rand = new Random();
		//on g�n�re un nombre pseudo-al�atoire entre 1000 et 10 000 inclus
		return ""+(rand.nextInt(9001)+1000);	
	}
    
	
	
}