package com.gbl.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import com.gbl.protocols.ProtocolServer;
import com.gbl.protocols.Protocols;
import com.gbl.protocols.signals.BreakProtocolSignal;
import com.gbl.protocols.signals.LeaveProtocolSignal;
import com.gbl.sessions.Session;
import com.gbl.utils.Message;
import com.gbl.utils.Show;



public class HandleClient implements Runnable
{

	private Thread thread; // contiendra le thread du client
	//private Socket _s; // recevra le socket liant au client
	private PrintWriter out; // pour gestion du flux de sortie
	private BufferedReader in; // pour gestion du flux d'entr�e
	private Server server; // pour utilisation des m�thodes de la classe principale
	private int _numClient=0; // contiendra le num�ro de client g�r� par ce thread
	private Session serverSession;
	
	HandleClient(Session _serverSession, Server _serv) // le param s est donn�e dans BlablaServ par ss.accept()
	{
		this.server = _serv;
		this.serverSession = _serverSession;
		this.out = serverSession.getOut();
		this.in = serverSession.getIn();
		
		_numClient = server.addClient(out);	// ajoute le flux de sortie dans la liste et r�cup�ration de son numero
		thread = new Thread(this); // instanciation du thread
		thread.start();
	}
	
	@Override
	public void run()
	{
		Show.trace("Nouvelle connexion: client "+_numClient);
		String message = "";
		boolean goGetProtocolWord = true;
			
		try
		{
			Show.trace("	Ready for Client "+_numClient);
			Message.sendStream(Message.createServerMessage(Protocols.StatusResponse.READY.toString()), out);
			ProtocolServer ps = new ProtocolServer(serverSession);
			
			while(message != null)
			{
				try
				{
//					while()
//					{
						//si protocol termin� -> break� le while
						if(goGetProtocolWord)
						{
							Show.test("handleClient","boucle while: WAITING protocolword",3);
							message = Protocols.getProtocolWord(in,serverSession);
							Show.test("handleClient","boucle while: GOT protocolword",3);
							if(message == null)
							{
								Show.test("handleClient","boucle while: message=null? : "+message,3);
								break;
								//throw new LeaveProtocolSignal();
							}
							
							Show.test("handleClient","boucle while: message="+message,3);
							if(!ps.launchProtocol(message))
							{
									Show.trace("Client "+_numClient+": ",message);
									message = "Serveur:: "+message;
									out.println(message);
									out.flush();
//									Message.sendStream(
//											Message.createClientMessage(message, serverSession.getLogin(), "Server", Protocols.Order.DEFAULT), 
//											out);
							}
						}
						goGetProtocolWord = true;
//					}
				}
				catch(BreakProtocolSignal e)
				{ 
					Show.trace("Client "+_numClient,e.getMessage());
					goGetProtocolWord = false;
				}
			}	
		}catch(LeaveProtocolSignal e) { 
			Show.trace("Client "+_numClient,e.getMessage());
//		}catch(Exception e) { 
//			Show.error("Client "+_numClient,e.getMessage()+e.getCause()+e.getClass()); 
		}
		finally
		{
			try
			{
				Show.trace("	Client "+_numClient," s'est deconnecte");
				server.delClient(_numClient); // on supprime le client de la liste
				serverSession.close(); // fermeture du socket si il ne l'a pas d�j� �t� (� cause de l'exception lev�e plus haut)
			}
			catch (IOException e)
			{ 
				Show.error("Client "+_numClient,"Can not close ServerSocket"); 
			}
		}
		
	}
}
